﻿'use strict';

angular
    .module('app')
    .controller('HomeController',
        ['UserService', '$rootScope', '$http', function (UserService, $rootScope, $http) {
            var vm = this,
                gravity = [0, 0, 0];

            vm.admin = false;
            vm.user = null;
            vm.allUsers = [];
            vm.deleteUser = deleteUser;
            vm.showGraph = showGraph;
            vm.isAdmin = function () {
                return vm.admin;
            };

            initController();

            function initController() {
                loadCurrentUser();
                loadAllUsers();
                hideGraphs();
            }

            function loadCurrentUser() {
                UserService.GetByUsername($rootScope.globals.currentUser.username)
                    .then(function (user) {
                        if (user.username === "admin") {
                            vm.admin = true;
                        }
                        vm.user = user;
                        loadTestData();
                    });
            }

            function loadAllUsers() {
                UserService.GetAll()
                    .then(function (users) {
                        var index = 0,
                            test = {
                                admin: false
                            };
                        _.each(users, function (user) {
                            if (user.username === "admin") {
                                test = {
                                    admin: true,
                                    index: index
                                };
                            }
                            index++;
                        });
                        if (test.admin) {
                            users.splice(test.index, 1)
                        }
                        vm.allUsers = users;
                    });
            }

            function loadTestData() {
                //todo dostać z BE nazwy pomiarów
                //var path = vm.user.username === "b" ? "data/majtanie1.json" : "data/lezacyBezObrobki3.json";
                //$http.get(path).success(function(data) {
                //    vm.availableOptions = [];
                //    for (var i = 0; i < data.length; i++) {
                //        console.log(data[i].name);
                //        vm.availableOptions.push({
                //            name: data[i].name
                //        });
                //    }
                //});
                vm.availableOptions = [
                    {name: "lezacyBezObrobki3"},
                    {name: "naPion"},
                    {name: "majtanie1"},
                    {name: "goraDolKilkaRazy"},
                    {name: "pojedynczyUpadek"},
                    {name: "majtanieX"},
                    {name: "majtanieY"},
                    {name: "majtanieZ"}
                ];
            }

            function deleteUser(id) {
                UserService.Delete(id)
                    .then(function () {
                        loadAllUsers();
                    });
            }

            function hideGraphs() {
                $('.js-graph').hide();
            }

            function showGraphs() {
                $('.js-graph').show();
            }

            function _lowPassFilter(t, dt, sample) {
                // alpha is calculated as t / (t + dT)
                // with t, the low-pass filter's time-constant
                // and dT, the event delivery rate

                var alpha = 0.8,
                    acceleration = [];

                gravity[0] = alpha * gravity[0] + (1 - alpha) * sample.x;
                gravity[1] = alpha * gravity[1] + (1 - alpha) * sample.y;
                gravity[2] = alpha * gravity[2] + (1 - alpha) * sample.z;

                acceleration[0] = sample.x - gravity[0];
                acceleration[1] = sample.y - gravity[1];
                acceleration[2] = sample.z - gravity[2];

                console.log(acceleration);
                return acceleration;
            }

            function showGraph() {
                var xTable = [],
                    yTableX = [],
                    yTableY = [],
                    yTableZ = [],
                    last_values = [],
                    velocity = [0, 0, 0],
                    path = [0, 0, 0],
                    globalPosition = [0, 0, 0],
                    last_timestamp = 0,
                    filePath = "data/" + $("#repeatSelect option:selected").text() + ".json",
                    dt,
                    acceleration;

                showGraphs();
                $http.get(filePath).success(function(data) {
                    console.log(data);
                    //todo api zwroci tablice czy konkretny pomiar?
                    //data = _.find(data, function (test) {
                    //    return test.name === $("#repeatSelect option:selected").text();
                    //});

                    var zeroTime = data.accelerometerTest.sampleList[0].sampleDate;

                    _.each(data.accelerometerTest.sampleList, function (sample) {
                        sample.sampleDate = (sample.sampleDate - zeroTime) / 1000; //milisekundy na sekundy

                        if (_.isEmpty(last_values)) {
                            last_values[0] = sample.x;
                            last_values[1] = sample.y;
                            last_values[2] = sample.z;
                        } else {
                            dt = (sample.sampleDate - last_timestamp);
                            acceleration = _lowPassFilter(1/data.accelerometerTest.frequency, dt, sample);

                            for (var i = 0; i < 3; i++) {
                                velocity[i] = (acceleration[i] + last_values[i])/2 * dt;
                                path[i] = velocity[i] * dt * 100;  //metry na centymetry
                                globalPosition[i] += path[i];
                                last_values[i] = acceleration[i]
                            }
                        }

                        last_timestamp = sample.sampleDate;

                        xTable.push(sample.sampleDate);
                        yTableX.push(globalPosition[0]);
                        yTableY.push(globalPosition[1]);
                        yTableZ.push(globalPosition[2]);
                    });
                });
                $rootScope.dataX = [{
                    x: xTable,
                    y: yTableX
                }];
                $rootScope.dataY = [{
                    x: xTable,
                    y: yTableY
                }];
                $rootScope.dataZ = [{
                    x: xTable,
                    y: yTableZ
                }];
                $rootScope.layoutX = {
                    height: 600,
                    width: 1000,
                    title: 'Oś X',
                    xaxis: {
                        title: 'Czas [s]'
                    },
                    yaxis: {
                        title: 'Przemieszczenie [cm]'
                    }
                };
                $rootScope.layoutY = {
                    height: 600,
                    width: 1000,
                    title: 'Oś Y',
                    xaxis: {
                        title: 'Czas [s]'
                    },
                    yaxis: {
                        title: 'Przemieszczenie [cm]'
                    }
                };
                $rootScope.layoutZ = {
                    height: 600,
                    width: 1000,
                    title: 'Oś Z',
                    xaxis: {
                        title: 'Czas [s]'
                    },
                    yaxis: {
                        title: 'Przemieszczenie [cm]'
                    }
                };
                $rootScope.options = {showLink: false, displayLogo: false};
            }
        }]
    );